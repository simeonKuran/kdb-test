#include "mainwindow.h"

#include "connection_widget.h"
#include "db_tables_widget.h"

#include <QApplication>
#include <QVBoxLayout>

#include <KMessageWidget>

#include <KDbDriver>
#include <KDbConnectionData>
#include <KDbConnection>
#include <KDbConnectionOptions>
#include <KDbQuerySchema>
#include <KDbMessageHandler>
#include <KDbCursor>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  m_connection(nullptr)
{
  // init ui:

  m_messageWidget = new KMessageWidget(this);
  m_messageWidget->hide();

  qApp->installEventFilter(this);

  m_connectionWidget = new ConnectionWidget(this);

  m_tableWidget = new DbTablesWidget(this);
  m_tableWidget->hide();

  QWidget* centralWidget = new QWidget(this);
  QVBoxLayout* layout = new QVBoxLayout(centralWidget);

  layout->addWidget(m_messageWidget);
  layout->addWidget(m_connectionWidget);
  layout->addWidget(m_tableWidget);

  setCentralWidget(centralWidget);

  connect(m_connectionWidget, &ConnectionWidget::databaseAvailable, this, &MainWindow::initDatabase);
  connect(m_connectionWidget, &ConnectionWidget::showMessage, this, &MainWindow::showMessage);

  connect(m_tableWidget, &DbTablesWidget::back, this, &MainWindow::back);
  connect(m_tableWidget, &DbTablesWidget::showMessage, this, &MainWindow::showMessage);
}

/*
void MainWindow::closeEvent(QCloseEvent* event)
{
}
*/

void MainWindow::initDatabase(QString dbName) // connection established:
{

  qDebug() << "init database..." << endl;

  m_connection = m_connectionWidget->getDBConnection();

  if (m_connection == nullptr)
    return;

  m_connectionWidget->hide();
  m_tableWidget->init(dbName, m_connection);
  m_tableWidget->show();
}

void MainWindow::back() // go back to the connection widget:
{
  m_tableWidget->hide();
  m_connectionWidget->show();
}

// to show warning / error messages:

void MainWindow::showMessage(KMessageWidget::MessageType type, const QString& message, bool showCloseButton, QAction* action /* = nullptr */)
{
  m_messageWidget->setText(message);
  m_messageWidget->setMessageType(type);
  m_messageWidget->setCloseButtonVisible(showCloseButton);

  if (action)
    m_messageWidget->addAction(action);

  m_messageWidget->animatedShow();
}

bool MainWindow::eventFilter(QObject *object, QEvent *event) // just used to reset Warnings / Error Messages
{
  Q_UNUSED(object);

  if (event->type() == QEvent::KeyPress || event->type() == QEvent::MouseButtonPress)
  {
    resetMessages();
  }

  return false; // allow this event to receive it's target...
}

void MainWindow::resetMessages()
{
  if (m_messageWidget->isVisible())
    m_messageWidget->animatedHide();
}
