#include "connection_widget.h"
#include "ui_connection_widget.h"

#include <QStringListModel>
#include <QMessageBox>

#include <KDbDriverManager>
#include <KDbDriver>
#include <KDbDriverMetaData>

#include <KDbConnectionData>
#include <KDbConnection>
#include <KDbConnectionOptions>

ConnectionWidget::ConnectionWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ConnectionWidget),
  m_connection(nullptr),
  m_driver(nullptr),
  m_driverManager(nullptr)
{
  // ui :
  ui->setupUi(this);
  ui->btnOk->setEnabled(false);

  initDBDrivers();

  m_databasesModel = new QStringListModel(this);

  ui->lvDatabases->setEditTriggers(QAbstractItemView::NoEditTriggers);
  ui->lvDatabases->setModel(m_databasesModel);

  connect(ui->btnConnect, &QPushButton::clicked, this, &ConnectionWidget::establishConnection);
  connect(ui->btnOk, &QPushButton::clicked, this, &ConnectionWidget::chooseDatabase);
  connect(ui->lePassword, &QLineEdit::editingFinished, this, &ConnectionWidget::establishConnection);
  connect(ui->lvDatabases->selectionModel(), &QItemSelectionModel::selectionChanged, this, &ConnectionWidget::dbSelected);
}

ConnectionWidget::~ConnectionWidget()
{
  delete ui;
  delete m_driverManager; // is not a QObject, so we need to delete it here!
}

KDbConnection* ConnectionWidget::getDBConnection() const
{
  return m_connection;
}

KDbDriver* ConnectionWidget::getDBDriver() const
{
  return m_driver;
}

void ConnectionWidget::dbSelected()
{
  if (ui->lvDatabases->currentIndex().isValid())
  {
    ui->btnOk->setEnabled(true);
  }
  else
  {
    ui->btnOk->setEnabled(false);
  }
}

void ConnectionWidget::establishConnection()
{
  // init DB connection:
  KDbConnectionData data;

  m_driverId = ui->cbDrivers->currentText();

  //get driver
  m_driver = m_driverManager->driver(m_driverId);

  if (!m_driver || m_driverManager->result().isError()) {
    qCritical() << "Error" << m_driverManager->result();
    errorRetrievingDrivers();
    return;
  }
  if (m_driverManager->driverMetaData(m_driverId)->isFileBased()) {
    qCritical() << "main: MIME types for" << m_driver->metaData()->id() << ":"
             << m_driver->metaData()->mimeTypes();
    QMessageBox::warning(this, "Error", "This db driver is file based and therefore not supported currently.");
    return;
  }

  data.setDriverId(m_driverId);
  data.setHostName(ui->leServer->text());
  data.setUserName(ui->leUsername->text());
  data.setPassword(ui->lePassword->text());
  data.setPort(ui->sbPort->value());

  KDbConnectionOptions options;
  options.setReadOnly(false);

  m_connection = m_driver->createConnection(data, options);

  if (m_connection->connect()) {
    qDebug() << "Connection successfully established." << endl;
  } else {
    qCritical() << "Failed to establish connection." << endl;
  }

  QStringList dbNames = m_connection->databaseNames();

  if (dbNames.isEmpty())
  {
    emit showMessage(KMessageWidget::Warning, "No Databases found. Try Again!", false, nullptr);
  }

  qDebug() << "db names: "  << dbNames << endl;

  m_databasesModel->setStringList(dbNames);
}

void ConnectionWidget::chooseDatabase()
{
  QModelIndex currentIndex = ui->lvDatabases->currentIndex();
  if (!currentIndex.isValid())
    return;

  QString database = currentIndex.data(Qt::DisplayRole).toString();

  const bool KEXI_COMPATIBLE = false;
  bool cancelled;

  bool ok = m_connection->useDatabase(database, KEXI_COMPATIBLE, &cancelled);

  if (ok)
  {
    qDebug() << "using DB '" << database << "'." << endl;

    emit databaseAvailable(database);
  }
  else
  {
    emit showMessage(KMessageWidget::Warning, "Using DB failed. Try again!", false, nullptr);
    qCritical() << "Failed to use DB '" << database << "'." << endl;
  }
}

void ConnectionWidget::initDBDrivers()
{
  m_driverManager = new KDbDriverManager(); // will delete drivers and connections on it's destructor!
  const QStringList driverIds = m_driverManager->driverIds();

  qDebug() << endl << "DRIVERS: " << driverIds << endl;

  if (driverIds.isEmpty()) {
    qCritical() << "No drivers found";
    errorRetrievingDrivers();
    return;
  }
  if (m_driverManager->result().isError()) {
    qCritical() << "Error: " << m_driverManager->result();
    errorRetrievingDrivers();
    return;
  }
\
  ui->cbDrivers->addItems(driverIds);
}

void ConnectionWidget::errorRetrievingDrivers()
{
  emit showMessage(KMessageWidget::Error, "No database driver found! Most probably the database plugins are missing or the plugin path is wrong.", true, nullptr);
}


