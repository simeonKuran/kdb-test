#ifndef DB_TABLES_WIDGET_H
#define DB_TABLES_WIDGET_H

#include <QWidget>

#include <QAction>
#include <KMessageWidget>


// fwd:
namespace Ui {
class DbTablesWidget;
}

class KDbConnection;

class DbTablesWidget : public QWidget
{
  Q_OBJECT

public:
  explicit DbTablesWidget(QWidget *parent = 0);
  virtual ~DbTablesWidget();

  void init(QString dbName, KDbConnection* connection);

private Q_SLOTS:
  void executeSql();

Q_SIGNALS:
    void back(void);

    void showMessage(KMessageWidget::MessageType type, const QString& str, bool ShowCloseButton, QAction* action);

private:
  Ui::DbTablesWidget *ui;
  KDbConnection* m_connection;
};

#endif // DB_TABLES_WIDGET_H
