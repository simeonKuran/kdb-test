#include "db_tables_widget.h"
#include "ui_db_tables_widget.h"

#include <QStringListModel>

#include <KDbConnection>
#include <KDbEscapedString>
// #include <KDbQuerySchema>
#include <KDbCursor>


DbTablesWidget::DbTablesWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::DbTablesWidget),
  m_connection(nullptr)
{
  ui->setupUi(this);

  connect(ui->btnBack, &QToolButton::clicked, this, &DbTablesWidget::back);
  connect(ui->btnExecuteQuery, &QPushButton::clicked, this, &DbTablesWidget::executeSql);
}

DbTablesWidget::~DbTablesWidget()
{
    delete ui;
}

void DbTablesWidget::init(QString dbName, KDbConnection* connection)
{
  ui->lbDatabase->setText("Database: " + dbName);

  m_connection = connection;

  ui->teQueryResult->clear();
}

void DbTablesWidget::executeSql()
{
  KDbEscapedString str(ui->leQuery->text());


  /*
  KDbTableSchema tableSchema("tabelle1");
  KDbQuerySchema schema(&tableSchema);

  KDbCursor* cursor = connection->executeQuery(&schema); // TODO: use KDbQuerySchema instead of the string..

  if (!cursor) {
    qDebug() << "Failed query: " << connection->recentSQLString() << endl;
    bool ok;
    qDebug() << "table names: " << connection->tableNames(false, &ok);
    qDebug() << "ok? : " << ok << endl;
    qDebug() << "cursor is invalid." << endl;
  }
  */

  // KDbCursor* cursor = m_connection->executeQuery(str); // TODO: use KDbQuerySchema instead of the string..

  /*
  KDbSqlResult* result = connection->executeSQL(str);
  // KDbResult kdbResult = connection->result();

  if (result) {
    qDebug() << "Result is valid. " << endl;
    delete result;
  } else {
    qDebug() << "Result is invalid." << endl;
  }
  */


  KDbCursor* cursor = m_connection->executeQuery(str);

  if (cursor)
  {
    QString result;

    if (cursor->moveFirst())
    {
      QByteArray group = "DB Entries";
      QByteArray key;

      do
      {
        int field_cnt = cursor->fieldCount();

        for (int i = 0; i < field_cnt; ++i)
        {
          QString fieldName = "field " + QString::number(i); // HOW TO retrieve field name ??
          QVariant value = cursor->value(i);
          result += fieldName + ": " + value.toString() + "\n";
        }

      } while(cursor->moveNext());
    }

    ui->teQueryResult->setText(result);

    m_connection->deleteCursor(cursor);
  }
  else
  {
    ui->teQueryResult->setText("Failed Query: " + m_connection->recentSQLString().toString() + "\nCursor is invalid.");

    emit showMessage(KMessageWidget::Error, "Failed Query: " + m_connection->recentSQLString().toString() + "\nCursor is invalid.", true, nullptr);
  }
}
