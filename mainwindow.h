#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <KMessageWidget>

// fwd:
class QLabel;
class QListView;
class QCloseEvent;

class ConnectionWidget;
class DbTablesWidget;

class KDbDriver;
class KDbConnection;

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  explicit MainWindow(QWidget *parent = 0);

protected:
  /*
  virtual void closeEvent(QCloseEvent* event) override;
  */
  bool eventFilter(QObject *object, QEvent *event) override;

public Q_SLOTS:
  void initDatabase(QString dbName);
  void back(void);

  void showMessage(KMessageWidget::MessageType type, const QString& message, bool showCloseButton, QAction* action = nullptr);

private:

  ConnectionWidget* m_connectionWidget;
  DbTablesWidget* m_tableWidget;

  KMessageWidget* m_messageWidget;

  KDbConnection* m_connection;
  KDbDriver* m_dbDriver;

  QLabel* m_dbLabel;
  QListView* m_dbTables;

  void resetMessages();
};

#endif // MAINWINDOW_H
