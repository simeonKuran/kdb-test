#include <QApplication>
#include <QtDebug>

#include "mainwindow.h"

int main(int argc, char *argv[])
{

  QApplication app(argc, argv);

  qDebug() << "library paths (used for plugins): " << QCoreApplication::libraryPaths() << endl;

  MainWindow window;
  window.show();

  return app.exec();
}
