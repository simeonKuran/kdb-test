#ifndef CONNECTION_WIDGET_H
#define CONNECTION_WIDGET_H

#include <QWidget>
#include <KMessageWidget>

// fwd
namespace Ui {
class ConnectionWidget;
}

class QStringListModel;
class KDbConnection;
class KDbDriver;
class KDbDriverManager;

class ConnectionWidget : public QWidget
{
  Q_OBJECT

public:
  explicit ConnectionWidget(QWidget *parent = 0);
  virtual ~ConnectionWidget();

  KDbConnection* getDBConnection() const;
  KDbDriver* getDBDriver() const;

private Q_SLOTS:
  void dbSelected(void);
  void establishConnection(void);
  void chooseDatabase(void);

Q_SIGNALS:
  void databaseAvailable(const QString& database);

  void showMessage(KMessageWidget::MessageType, const QString& message, bool showCloseButton, QAction* action);

private:
  Ui::ConnectionWidget *ui;
  KDbConnection* m_connection;
  KDbDriver* m_driver;
  KDbDriverManager* m_driverManager;
  QString m_driverId;
  QStringListModel* m_databasesModel;

  void initDBDrivers();
  void errorRetrievingDrivers();
};

#endif // CONNECTION_WIDGET_H
